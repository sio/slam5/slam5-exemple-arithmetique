###########
# TARGETS #
###########
SUBDIRS = lib src
.PHONY: all $(SUBDIRS) clean

all: | $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@

# Règles de nettoyage
clean:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir clean; \
	done
